
## Entrenamiento py

Por Leonardo TANA


![altonda](imag/lpm.png  "onda")

Se planifica los siguientes temas

| Sesión | Tema                                                 | Fecha      | Hora          |
| ------ | ---------------------------------------------------- | ---------- | ------------- |
| 1      | ¿Qué es el lenguaje python?                          | 2024-06-04 | 11:30 a12:20  |
| 2      | ¿Qué herramientas o IDE se emplea?                   | 2024-06-07 | 10:00 a 11:00 |
| 3      | Instalación y configuración de un contenedor python3 | 2024-06-07 | 14:00 a 15:00 |
| 4      | Paquetes para manejo de BDD y análisis de datos      | 2024-06-10 | 10:00 a 11:00 |
| 5      | Estructura Datos                                     | 2024-06-11 | 10:00 a 11:00 |
|        |                                                      |            |               |
|        |                                                      |            |               |
|        |                                                      |            |               |
|        |                                                      |            |               |
|        |                                                      |            |               |
|        |                                                      |            |               |
|        |                                                      |            |               |
|        |                                                      |            |               |
|        |                                                      |            |               |
|        |                                                      |            |               |

